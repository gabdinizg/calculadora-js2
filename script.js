let n1, n2, operador, resultado;

function adicionarNumero(numberParam){
    if(n1){
        n2 = Number(numberParam);
        calcular();
    } else{
        n1 = Number(numberParam);
    }

    //alert(`entrou aqui ${numberParam}`)
}

function adicionarOperador(operadorParam){
   operador = operadorParam;
}

function calcular(){
    switch (operador) {
        case "+":
            soma();
            break;
        case "-":
            subtracao()
            break;
        case "*":
            multiplicacao();
            break;
        case "/":
            divisaoreal();
            break;
        default:
            alert("escolha uma operação válida");
            rest();
            break;
    }
}

function rest(params) {
    n1 = undefined;
    n2 = undefined;
    operador = undefined;
    resultado = undefined;
}

function soma() {
    resultado = n1 + n2;
    alert(`${n1} + ${n2} = ${resultado}`);
    rest();
}

function subtracao() {
    resultado = n1 - n2;
    alert(`${n1} - ${n2} = ${resultado}`);
    rest();
}

function multiplicacao() {
    resultado = n1 * n2;
    alert(`${n1} * ${n2} = ${resultado}`);
    rest();
}

function divisaoreal() {
    resultado = n1 / n2;
    alert(`${n1} / ${n2} = ${resultado}`);
    rest();
}



